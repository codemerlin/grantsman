(function() {
    var animations = {
        text: 'Forms',
        card: false,
		leaf: true,
		id: 'forms'
		};

    var root = {
        id: 'root',
        text: 'HRSA EHB Module',
        items: [
            {
                text: 'Contacts',
                id: 'ui',
                cls: 'launchscreen',
                items: [
                    {
                        text: 'Buttons',
                        leaf: true,
                        id: 'buttons'
                    },
                    {
                        text: 'Forms',
                        leaf: true,
                        id: 'forms'
                    },
                    {
                        text: 'List',
                        leaf: true,
                        id: 'list'
                    },
                    {
                        text: 'Nested List',
                        view: 'NestedList',
                        leaf: true,
                        id: 'nestedlist'
                    },
                    {
                        text: 'Icons',
                        leaf: true,
                        id: 'icons'
                    },
                    {
                        text: 'Toolbars',
                        leaf: true,
                        id: 'toolbars'
                    },
                    {
                        text: 'Carousel',
                        leaf: true,
                        id: 'carousel'
                    },
                    {
                        text: 'Tabs',
                        leaf: true,
                        id: 'tabs'
                    },
                    {
                        text: 'Bottom Tabs',
                        view: 'BottomTabs',
                        leaf: true,
                        id: 'bottom-tabs'
                    },
                    {
                        text: 'Overlays',
                        leaf: true,
                        id: 'overlays'
                    }
                ]
            }
        ]
    };

    //Ext.Array.each(animations, function(anim) {
    //    root.items.push(anim);
    //});

    root.items.push(animations, {
        text: 'Submissions',
        id: 'carousel',
        view: 'Carousel',
        leaf: true
    }, {
        text: 'Documents',
        id: 'data',
        view: 'Icons',
		leaf: true
    }, {
        text: 'Prior Approvals',
		view: 'NestedList',
		leaf: true,
		id: 'nestedlist'
    });

    Ext.define('Kitchensink.store.Demos', {
        alias: 'store.Demos',
        extend: 'Ext.data.TreeStore',
        requires: ['Kitchensink.model.Demo'],

        config: {
            model: 'Kitchensink.model.Demo',
            root: root,
            defaultRootProperty: 'items'
        }
    });
})();
